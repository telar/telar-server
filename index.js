/*!
 * telar-server
 * Copyright(c) 2017 Telar Team
 * ISC Licensed
 */

'use strict';

module.exports = require('./src/Server');
