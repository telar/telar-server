const assert = require('assert');

const Handler = require('../../src/Handler');
const Controller = require('../../src/Controller');

let handler;

describe('Handler', function() {

  before(function(){
    handler = new Handler('Main').instance;
  });

  describe('#instance()', function() {
    it('should return a Controller instance', function() {
      assert(handler instanceof Controller);
    });

    it('should have the default CRUD methods declared', function() {
      assert(handler.hasOwnProperty('index'));
      assert(handler.hasOwnProperty('create'));
      assert(handler.hasOwnProperty('show'));
      assert(handler.hasOwnProperty('store'));
      assert(handler.hasOwnProperty('edit'));
      assert(handler.hasOwnProperty('update'));
      assert(handler.hasOwnProperty('destroy'));
    });
  });
});
