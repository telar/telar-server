const assert = require('assert');

const Config = require('../../src/Config');
const defaults = require('../../src/config/config');

let config;

describe('Config', function() {

  before(function(){
    config = new Config();
  });

  describe('#host()', function() {
    it('should return a default value when is not defined', function() {
      assert.equal(config.host, defaults.host);
    });

    it.skip('should return a new host when the setter function is called', function(){});
  });

  describe.skip('#environment()', function(){
    it('should return a default value when is not defined', function() {});
    it('should return a new value when the setter function is called', function(){});
  });

  describe.skip('#port()', function(){
    it('should return a default value when is not defined', function() {});
    it('should return a new value when the setter function is called', function(){});
  });
});
