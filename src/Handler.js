'use strict';

const fs = require('fs');
const detect = require('async/detect');
const every = require('async/every');
const setImmediate = require('async/setImmediate');

const Controller = require('./Controller');

module.exports = class Handler {
  constructor(object, directory='controllers') {
    this.directory = directory;
    this.instance  = object;
  }

  set directory(directory){ this._directory = directory.replace('/',''); }

  get directory() { return this._directory; }

  set instance(instance) {
    let self = this;

    if (typeof instance == 'string') {
      self.fromPath(instance, self.directory, function(error, transformed) {
        self._instance = transformed;
      });
    } else {
      self._instance = self.fromObject(instance || {});
    }
  }

  get instance() { return this._instance; }

  fromPath(path, directory, callback) {
    let Transformed = {};
    let file = `${directory}/${path}`;
    let error = null;
    let fromObject = this.fromObject;

    // TODO: Remove the detect method, replace it with directory method
    detect([__dirname, process.cwd()], function(dirPath, found) {
      if (fs.existsSync(`${dirPath}/${file}.js`)) found(error, true);
    }, function(error, currentDirectory) {
      if (error) {
        throw new Error(`The handler: ${path} doesn't exist on: ${currentDirectory}`);
      } else {
        let _Object = require(`${currentDirectory}/${file}`);
        let Instance = new _Object;

        every(Object.getOwnPropertyNames(_Object.prototype), function(method, next) {
          if (method !== 'constructor')
            Transformed[method] = Instance[method];
          next(error, Transformed);
        }, function(error, result) {
          if ((! error) && result)
            callback(error, fromObject(Transformed));
          else
            callback('Something weird happend', null);
        });
      }
    });
  }

  fromObject(object) {
    return Object.assign(new Controller, object);
  }
}
