'use strict';

const defaults = require('./config/config');

module.exports = class Config {
  constructor(params={}) {
    this.host = params.host;
    this.port = params.port;
    this.router = params.router;
    this.environment = params.environment;
  }

  get host() { return this._host; }

  set host(host) { this._host = host || defaults.host; }

  get port() { return this._port; }

  set port(port) { this._port = parseInt((port || defaults.port)); }

  get environment() { return this._environment; }

  set environment(environment){ this._environment = environment || defaults.environment; }

  get router() { return this._router; }

  set router(router) { this._router = router || defaults.router; }
}
