'use strict';

module.exports = class Controller {
	constructor(){
		this.index = function(){
			this.ok('hello from index');
		};

		this.create = function(){
			this.ok('hello from create');
		};

		this.show = function(index){
			this.ok('hello from show: ' + index);
		};

		this.store = function(){
			this.ok('hello from store with params: ' + JSON.stringify(this.input));
		};

		this.edit = function(index){
			this.ok('hello from edit: ' + index);
		};

		this.update = function(index){
			this.ok('hello from update: ' + index + ', with params: ' + JSON.stringify(this.input));
		};

		this.destroy = function(index){
			this.ok('hello from delete: ' + index);
		};
	}
};
