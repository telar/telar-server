'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const Router = require('./Router');
const Logger = require('./Logger');
const Config = require('./Config');

const each = require('async/each');

module.exports = class Telar {
  constructor(params={}) {
    this.config = new Config(params);
    this.logger = new Logger(this._config);
    this.router = new Router({}, this.config);
    this.server = express();
  }

  set config(config) {
    if (! config instanceof Config)
      throw new Error('Config is not instance of Telar');
    this._config = config;
  }

  get config() { return this._config; }

  set router(router) {
    if (! router instanceof Router)
      throw new Error('Config is not instance of Telar');
    this._router = router;
  }

  get router() { return this._router; }

  set logger(logger) {
    if (! logger instanceof Logger)
      throw new Error('Logger is not instance of Telar');
    this._logger = logger;
  }

  get logger() { return this._logger; }

  set server(server) { this._server = server; }

  get server() { return this._server; }

  init() {
    let server = this.server;
    let logger = this.logger;
    let router = this.router;
    // Parser
    server.use(bodyParser.urlencoded({ extended: false })); // application/x-www-form-urlencoded
    server.use(bodyParser.json()); // application/json
    // Router Middleware
    server.use(router.middleware);
    // Declare Routes
    router.routes(function(err, routes) {
      if (! err) {
        each(routes, function(route, callback) {
          server.use(route.pattern, route.handler);
          callback();
        }, function(error) {
          if (error) throw new Error(error);
        });
      } else {
        throw new Error(err);
      }
    });
    // Logger Middleware
    server.use(logger.middleware);
  }

  start() {
    let server = this.server;
    let shout = this.logger.shout;
    let port = this.config.port;

    this.init();

    server.listen(port, function(){
      shout('Telar.js is running');
      shout('Listening on port: ' + port);
    });
  }

  /** Controllers and routes are registered in Telar instance
  * There is a loader for each controller and a cache, that can be reloaded
  * */
  addRoute() {}

  deleteRoute() {}

  addService() {}

  loadService() {}

  reloadService() {}

  deleteService() {}

  addResource(pattern, handler) { this.server.use(pattern, handler); }

  addController() {}

  loadController() {}

  reloadController() {}

  deleteController() {}

  addView() {}

  loadView() {}

  reloadView() {}

  parseView() {}

  updateView() {} // Does this make sense?

  deleteView() {}
}
