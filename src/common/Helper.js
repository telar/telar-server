'use strict';

const _set = require('lodash/set');
const _get = require('lodash/get');
const _forOwn = require('lodash/forOwn');

class Helper {

  static exists(value) {
    return typeof value !== 'undefined' && value !== null;
  }

  static assign(...values) {
    return Object.assign.apply(null, values);
  }

  static set(object, key, value) {
    return _set(object, key, value);
  }

  static get(object, key) {
    return _get(object, key);
  }

  static forOwn(object, callback){
    return _forOwn(object, callback);
  }
}

module.exports = Helper;
