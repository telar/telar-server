'use strict';

const winston = require('winston');

const Config = require('./Config');

module.exports = class Logger {
  constructor(config) {
    this.config = config;
  }

  get config() { return this._config; }

  set config(config) {
    this._config = (config instanceof Config) ? config: new Config;
  }

  get middleware() {
    let shout = this.shout;

    return function(req, res, next){
      shout('Request made from: ' + req.url);
      next();
    };
  }

  shout(msg) {
    return winston.info(msg);
  }
}
