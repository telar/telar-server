'use strict';

const express = require('express');

const Response = require('./Response');
const Handler = require('./Handler');

module.exports = class Route {
	constructor(verb, handler, pattern='', directory='controllers', method=null) {
		this.router = express.Router();

		if (typeof handler == 'string') {
			let abstract = handler.split('.');
			if (abstract.length == 3) {
				[this.directory, this.handler, this.method] = abstract;
			} else {
				[this.handler, this.method] = abstract;
				directory = 'controllers';
			}
		}

		this.pattern = pattern;
		this.verb = verb;

    let callback =  this.handler[this.method];

    this.router[this.verb](this.pattern, function(req, res){
    	let params = Object.keys(req.params).map(key => req.params[key]);

      return callback.call(new Response(req, res), params);
    });
	}

	set method(method) { this._method = method; }

	get method() { return this._method; }

	set router(router) { this._router = router; }

	get router() { return this._router; }

	set verb(verb) { this._verb = ( verb || 'GET' ).toLowerCase(); }

	get verb() { return this._verb; }

	set pattern(pattern) { this._pattern = pattern !== '/' ? pattern : ''; }

	get pattern() { return this._pattern; }

  set handler(object) { this._handler = new Handler(object).instance; }

  get handler() { return this._handler; }

	set directory(directory) { this._directory = directory; }

	get directory() { return this._directory; }
}
