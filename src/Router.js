'use strict';

const transform = require('async/transform');
const setImmediate = require('async/setImmediate');
<<<<<<< HEAD
=======
const path = require('path');
>>>>>>> b9da999b1b49f4f64040191af3219acdd5eb7c74
const express = require('express');

const Config = require('./Config');
const Logger = require('./Logger');
const Resource = require('./Resource');
const Route = require('./Route');

const defaults = require('./config/routes');

module.exports = class Router {
  constructor(routes, config, logger) {
    this.config = config;
    this.logger = logger;
    this._routes = routes;
  }

  routes(callback) {
    let config = this.config.router;

    return transform(Object.assign(defaults, this._routes), function(obj, value, key, next) {
      let type = value.type || config.type;
      let verb = value.verb || config.verb;
      let handler = value.handler || config.handler;

      setImmediate(function() {
        switch (type) {
          case 'resource':
            obj[key] = { pattern: key, type: type, handler: new Resource(handler).router };
            break;
          case 'route':
            obj[key] = { pattern: key, type: type, handler: new Route(verb, handler).router };
            break;
          case 'static':
            // Relative to cwd https://expressjs.com/en/starter/static-files.html
            obj[key] = { pattern: key, type: type, handler: express.static(handler)};
            break;
          default:
            throw new Error(`Route ${type} not defined.`);
        }
        next();
      });
    }, callback);
  }

  set config(config) { this._config = (config instanceof Config) ? config : new Config; }

  get config() { return this._config; }

  set logger(logger) { this._logger = (logger instanceof Logger) ? logger : new Logger; }

  get logger() { return this._logger; }

  get middleware () {
    let shout = this.logger.shout;
    let handler = this.handler;
    let routes = this.routes;

    return function callback(req, res, next) {
      next();
    };
  }
}
