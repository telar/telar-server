'use strict';

module.exports = class Response {
	constructor(req, res) {
		this.req = req;
		this.res = res;
	}

	ok(output) {
		return this.res.status(200).send(output);
	}

	input(name) {
		return this.req.body[name] || false;
	}

	get input() { return this.req.body; }
}
