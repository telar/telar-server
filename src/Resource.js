'use strict';

const express = require('express');
const every = require('async/every');

const Response = require('./Response');
const Handler = require('./Handler');

module.exports = class Resource {
  constructor(handler, pattern=null, directory='controllers') {
    this.router = express.Router();
    this.directory = directory;
    this.handler = handler;
    this.pattern = pattern;

    this.defaults = [
      {verbs: 'GET', pattern: this.pattern, handler: this.handler, method: 'index'},
      {verbs: 'GET', pattern: `${this.pattern}/create`, handler: this.handler, method: 'create'},
      {verbs: 'POST', pattern: this.pattern, handler: this.handler, method: 'store'},
      {verbs: 'GET', pattern: `${this.pattern}/:id`, handler: this.handler, method: 'show'},
      {verbs: 'GET', pattern: `${this.pattern}/:id/edit`, handler: this.handler, method: 'edit'},
      {verbs: 'PUT', pattern: `${this.pattern}/:id`, handler: this.handler, method: 'update'},
      {verbs: 'DELETE', pattern: `${this.pattern}/:id`, handler: this.handler, method: 'destroy'}
    ];
  }

  set router(router) { this._router = router; }

  get router() { return this._router; }

  set directory(directory) { this._directory = directory; }

  get directory() { return this._directory; }

  set pattern(pattern) {  this._pattern = (pattern !== '/' && pattern !== null) ? pattern : ''; }

  get pattern() { return this._pattern; }

  set handler(object) { this._handler = new Handler(object, this.directory).instance; }

  get handler() { return this._handler; }

  set defaults(multiPaths) {
    let self = this;
    every(multiPaths, function(object, callback) {
      let method = object.method;
      let handler = object.handler;
      let pattern = object.pattern;
      let verb = object.verbs.toLowerCase();

      callback(null, self.router[verb](pattern, function(req, res) {
        let params = Object.keys(req.params).map(key => req.params[key]);

        return handler[method].call(new Response(req,res), params);
      }));
    }, function(err, result) {
        // if result is true then every route is correct
        if (err) {
          throw new Error(err);
        }
    });
  }
}
