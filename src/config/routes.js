/******************************************************************************
* System Routes                                                           *
*-----------------------------------------------------------------------------*
* Here will be the routes middleware for creating resources and services.     *
******************************************************************************/

module.exports = {
  '/': {
    verb: 'GET',
    handler: 'Main.index'
  },
  '/space': {
    type: 'resource',
    handler: 'Space'
  },
  '/static': {
    type: 'static',
    handler: 'public'
  }
};
