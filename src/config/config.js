module.exports = {
  host: 'http://localhost',
  port: 1337,
  environment: 'development',
  api: { version: 'v1' },
  router: { verb: 'get', type: 'route', handler: {} }
};
