function ctx(contextClass, fn){
  let methods = Object.getOwnPropertyNames(contextClass.prototype);

  for (let i = 0; i < methods.length; i++) {
    if (methods[i] !== 'constructor') {
      this[ methods[i] ] = contextClass.prototype[methods[i]];
    }
  }

  fn.call(this);
}

module.exports = class Contextualizer {
  constructor(contextClass, params) {
    this.contextClass = contextClass;


  }

  run(fn) {
    ctx(this.contextClass, fn);
  }
}
